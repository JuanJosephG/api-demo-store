package rc.demostore.persistence;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.MongoTemplate;
import rc.demostore.model.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;

@Service
public class DbSeeder implements CommandLineRunner {
    private PuzzleSetRepository puzzleSetRepository;
    private MongoTemplate mongoTemplate;

    public DbSeeder(PuzzleSetRepository puzzleSetRepository, MongoTemplate mongoTemplate) {
        this.puzzleSetRepository = puzzleSetRepository;
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        this.puzzleSetRepository.deleteAll();

        /*
        Payment Options
         */
        PaymentOptions creditCardPayment = new PaymentOptions(PaymentType.CreditCard, 0);
        PaymentOptions payPalPayment = new PaymentOptions(PaymentType.PayPal, 1);
        PaymentOptions cashPayment = new PaymentOptions(PaymentType.Cash, 10);
        this.mongoTemplate.insert(creditCardPayment);
        this.mongoTemplate.insert(cashPayment);
        this.mongoTemplate.insert(payPalPayment);

        /*PuzzleSet*/

        PuzzleSet millenniumFalcon = new PuzzleSet(
                "Millennium Falcon",
                PuzzleSetDifficult.HARD,
                "Star Wars",
                Arrays.asList(
                        new ProductReview("Astrid", 7 ),
                        new ProductReview("Felipe", 8),
                        new ProductReview("Richard", 9)
                ),
                new DeliveryInfo(LocalDate.now().plusDays(1), 30, true),
                creditCardPayment);

        PuzzleSet skyPolice = new PuzzleSet(
                "Sky Police Air Base",
                PuzzleSetDifficult.MEDIUM,
                "City",
                Arrays.asList(
                        new ProductReview("Carl", 5 ),
                        new ProductReview("Omar", 8)
                ),
                new DeliveryInfo(LocalDate.now().plusDays(3), 50, true),
                creditCardPayment);

        PuzzleSet mcLarenUltimate = new PuzzleSet(
                "McLarenUltimate",
                PuzzleSetDifficult.EASY,
                "Speed Champions",
                Arrays.asList(
                        new ProductReview("Gabriel", 9 ),
                        new ProductReview("Bosco", 10),
                        new ProductReview("Christ", 8)
                ),
                new DeliveryInfo(LocalDate.now().plusDays(7), 70, false),
                payPalPayment);

        PuzzleSet effielParis = new PuzzleSet(
                "effielParis",
                PuzzleSetDifficult.EASY,
                "Towers",
                Arrays.asList(
                        new ProductReview("Daniel", 9 ),
                        new ProductReview("Edurne", 7),
                        new ProductReview("Susna", 6 )
                ),
                new DeliveryInfo(LocalDate.now().plusDays(10), 100, false),
                cashPayment);

        Collection<PuzzleSet> initialPuzzles = Arrays.asList(millenniumFalcon,
                skyPolice,
                mcLarenUltimate,
                effielParis);
        this.puzzleSetRepository.insert(initialPuzzles);
    }
}
