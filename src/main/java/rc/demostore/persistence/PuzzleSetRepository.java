package rc.demostore.persistence;

import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import rc.demostore.model.PuzzleSet;
import rc.demostore.model.PuzzleSetDifficult;

import java.util.Collection;
import java.util.function.Predicate;

@Repository
public interface PuzzleSetRepository extends MongoRepository <PuzzleSet, String>, QuerydslPredicateExecutor<PuzzleSet> {
    Collection<PuzzleSet> findAllByThemeContains(String theme);
    Collection<PuzzleSet> findAllByDifficultyAndNameStartsWith(PuzzleSetDifficult difficulty, String name);
    Collection<PuzzleSet> findAllBy(TextCriteria textCriteria);

    @Query("{'delivery.deliveryFee' : {$lt : ?0}}")
    Collection<PuzzleSet> findAllDeliveryPriceLessThan(int price);

    @Query("{'reviews.rating' : {$eq : 10}}A")
    Collection<PuzzleSet> findAllByGreatReviews();
}
