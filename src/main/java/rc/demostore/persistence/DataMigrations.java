package rc.demostore.persistence;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import rc.demostore.model.PuzzleSet;

@ChangeLog(order = "001")
public class DataMigrations {

    @ChangeSet(order = "001", author = "dan", id= "update nb part")
    public void updateNbPart(MongoTemplate mongoTemplate){
        Criteria prizeZeroCriteria = new Criteria().orOperator(
                Criteria.where("nbParts").is(0),
                Criteria.where("nbParts").is(null));

        mongoTemplate.updateMulti(
                new Query(prizeZeroCriteria),
                Update.update("nbParts", 122),
                PuzzleSet.class);

        System.out.println("Applied changet 001");
    }
}
