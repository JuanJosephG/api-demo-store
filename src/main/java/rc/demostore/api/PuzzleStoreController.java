package rc.demostore.api;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Text;
import rc.demostore.model.PuzzleSet;
import rc.demostore.model.PuzzleSetDifficult;
import rc.demostore.model.QPuzzleSet;
import rc.demostore.persistence.PuzzleSetRepository;

import java.util.Collection;

@RestController
@RequestMapping("puzzlestore/api")
public class PuzzleStoreController {
    private PuzzleSetRepository puzzleSetRepository;

    public PuzzleStoreController(PuzzleSetRepository puzzleSetRepository){ this.puzzleSetRepository = puzzleSetRepository; }

    @PostMapping
    public void insert(@RequestBody PuzzleSet puzzleSet){ this.puzzleSetRepository.insert(puzzleSet); }

    @PutMapping
    public void update(@RequestBody PuzzleSet puzzleSet){ this.puzzleSetRepository.save(puzzleSet); }

    @DeleteMapping("{/{id}")
    public void delete(@RequestBody String id){ this.puzzleSetRepository.deleteById(id); }

    @GetMapping("/all_puzzle")
    public Collection<PuzzleSet> all(){
        Sort sortByNameAsc = Sort.by("name").descending();
        Collection<PuzzleSet> puzzlesets = this.puzzleSetRepository.findAll(sortByNameAsc);
        return puzzlesets;
    }

    @GetMapping("/{id}")
    public PuzzleSet byId(@PathVariable String id){
        PuzzleSet puzzleSet = this.puzzleSetRepository.findById(id).orElse(null);
        return puzzleSet;
    }

    @GetMapping("/byTheme/{theme}")
    public Collection<PuzzleSet> byTheme(@PathVariable String theme){
        return this.puzzleSetRepository.findAllByThemeContains(theme);
    }

    @GetMapping("/hardThatStartWithM")
    public Collection<PuzzleSet> hardThatStartWithM(){
        return this.puzzleSetRepository.findAllByDifficultyAndNameStartsWith(PuzzleSetDifficult.HARD, "M");
    }

    @GetMapping("/ByDeliveryFeeLessThan/{price}")
    public Collection<PuzzleSet> byDeliveryFeeLessThan(@PathVariable int price){
        return this.puzzleSetRepository.findAllDeliveryPriceLessThan(price);
    }

    @GetMapping("/greatReviews")
    public Collection<PuzzleSet> byGreatReviews(){
        return this.puzzleSetRepository.findAllByGreatReviews();
    }

    @GetMapping("bestBuys")
    public Collection<PuzzleSet> bestBuys() {
        //build the query
        QPuzzleSet query = new QPuzzleSet("query");
        BooleanExpression inStockFilter = query.deliveryInfo.inStock.isTrue();
        Predicate smallDeliveryFeeFilter = query.deliveryInfo.deliveryFee.lt(50);
        Predicate hasGreatReviews = query.reviews.any().rating.eq(10);

        Predicate bestBuysFilter = inStockFilter.and(smallDeliveryFeeFilter).and(hasGreatReviews);

        return (Collection<PuzzleSet>) this.puzzleSetRepository.findAll(bestBuysFilter);
    }

    @GetMapping("fullTextSearch/{text}")
    public Collection<PuzzleSet> fullTextSearch(@PathVariable String text){
        TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching(text);
        return this.puzzleSetRepository.findAllBy(textCriteria);
    }
}
