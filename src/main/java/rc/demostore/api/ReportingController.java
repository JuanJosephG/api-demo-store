package rc.demostore.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rc.demostore.model.AvgRatingModel;
import rc.demostore.persistence.ReportingService;

import java.util.List;

@RestController
@RequestMapping("/puzzlestore/api/reports")
public class ReportingController {
    private ReportingService reportingService;

    public ReportingController(ReportingService reportingService) {
        this.reportingService = reportingService;
    }

    @GetMapping("avgRatingsReport")
    public List<AvgRatingModel> avgRatingModelList(){
        return this.reportingService.getAvgRatingReport();
    }
}
