package rc.demostore.model;

public enum PaymentType {
    CreditCard,
    PayPal,
    Cash
}