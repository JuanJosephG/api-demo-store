package rc.demostore.model;

public class AvgRatingModel {
    private String id;
    private String productName;
    private double avgRating;

    public double getAvgRating(){
        return avgRating;
    }

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }
}
