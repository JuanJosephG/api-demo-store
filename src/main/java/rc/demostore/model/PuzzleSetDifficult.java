package rc.demostore.model;

public enum PuzzleSetDifficult {
    NOT_AVAILABLE,
    EASY,
    MEDIUM,
    HARD,
}
