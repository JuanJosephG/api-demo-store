package rc.demostore;

import com.github.mongobee.Mongobee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

@SpringBootApplication
public class PuzzleStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(PuzzleStoreApplication.class, args);
	}

	@Autowired
	MongoTemplate mongoTemplate;
	@Bean
	public Mongobee mongobee(){
		Mongobee runner = new Mongobee("mongodb://localhost:27017/puzzlestore");
		runner.setMongoTemplate(mongoTemplate);         // host must be set if not set in URI
		runner.setChangeLogsScanPackage("rc.demostore.persistence"); // the package to be scanned for changesets

		return runner;
	}
}
