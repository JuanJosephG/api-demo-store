package rc.demostore;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import rc.demostore.model.*;
import rc.demostore.persistence.PuzzleSetRepository;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataMongoTest
public class PuzzlestoreDatabaseTest {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private PuzzleSetRepository puzzleSetRepository;

	@Before
	public void before(){
		this.puzzleSetRepository.deleteAll();

		PuzzleSet millenniumFalcon = new PuzzleSet("Millennium Falcon",
				PuzzleSetDifficult.HARD,
				"Star Wars",
				Arrays.asList(
						new ProductReview("Astrid", 7),
						new ProductReview("Felipe", 10),
						new ProductReview("Richard", 9)
				),
				new DeliveryInfo(LocalDate.now().plusDays(1), 30, true),
				new PaymentOptions(PaymentType.CreditCard, 0));

		PuzzleSet skyPolice = new PuzzleSet(
				"Sky Police Air Base",
				PuzzleSetDifficult.MEDIUM,
				"City",
				Arrays.asList(
						new ProductReview("Carl", 5 ),
						new ProductReview("Omar", 8)
				),
				new DeliveryInfo(LocalDate.now().plusDays(3), 50, true),
				new PaymentOptions(PaymentType.CreditCard, 0));

		this.puzzleSetRepository.insert(millenniumFalcon);
		this.puzzleSetRepository.insert(skyPolice);
	}

	@Test
	public void findAllByGreatReviews_should_return_product_that_have_a_review_with_a_rating_of_ten() {
		List<PuzzleSet> results = (List<PuzzleSet>) this.puzzleSetRepository.findAllByGreatReviews();
		assertEquals(1, results.size());
		assertEquals("Millennium Falcon", results.get(0).getName());

	}

}
