# PUZZLE-API

Puzzle API is project develop in MongoDB and SprintBoot

## Getting Started

### Prerequisites
* Java 13
* MongoDB 4.2.3 Community
* Robot 3T
* IntelliJ IDEA 2019.3.2 (Community Edition)

1) To Start:
```
git clone https://gitlab.com/JuanJosephG/api-demo-store.git
```

2) Start Mongo Service

3) Run the project in IntelliJ

If you want to check the different endpoints, Please download the postman.json 
